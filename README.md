# SOS-GP Server API #

Este projeto está sendo desenvolvido em Java (com arquitetura REST JAX-RS) e corresponde ao lado servidor do sistema SOS-GP.

O sistema SOS-GP tem como objetivo permitir que deficientes auditivos façam chamadas de emergência e urgência ao Corpo de Bombeiros da região de Guarapuava por meio de mensagens de texto, envio de imagens e o envio da sua localização através de GPS.

### Principais Funcionalidades ###

* Cadastro de usuários com comprovada necessidade especial.
* Cadastro do histórico de saúde dos usuários.
* Identificação geográfica da vítima.
* Acompanhamento geográfico da ambulância pela vítima ou responsáveis.
* Recursos de comunicação da vítima com atendentes do Corpo de Bombeiros.

### Tecnologias ###

* Java
* JPA (Hibernate)
* JAX-RS (Jersey)

### Ferramentas ###

* Eclipse
* MySQL
* Tomcat
* Maven

### Manual de Execução ###

+ Clonar o repositório com `git clone`
+ Abrir o projeto no Eclipse
+ Instalar as dependência definidas no arquivo `pom.xml` do Maven
    - Entrar na raiz do projeto via prompt de comando: `cd sosgp-api`
    - Digite o comando `mvn clean install`
+ Criar um novo banco de dados no MySQL com o nome `sos_db`
+ Editar o arquivo `src/main/resources/META-INF/persistence.xml` com as configurações de acesso ao banco de dados `sos_db`
+ Alterar a propriedade `javax.persistence.jdbc.user` para o usuário correspondente ao MySQL
+ Alterar a propriedade `javax.persistence.jdbc.password` para a senha correspondente ao MySQL
+ Realizar o deploy do projeto no Tomcat.   


### URLs ###
+ URL: `/` Método: `get`- retorna os estados de todos os pagers
+ URL `/:id` - Método: `get` - retorna o estado do pager correspondente ao id informado. Exemplo: `localhost/1` 
+ URL `/:id` - Método: `post` - recebe um JSON com dados de uma ação a ser realizada. Exemplo: `localhost/1` JSON: `{action: 'disable'}` 

### Versão Corrente ###
0.0.1 - 21/07/2016