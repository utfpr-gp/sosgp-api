package org.gp.sos.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Persistence;

import org.gp.sos.model.City;
import org.gp.sos.model.Neighborhood;
import org.gp.sos.model.service.CityService;

public class SchemaCreator {

	public static void main(String[] args) {
		
		SchemaCreator schemaCreator = new SchemaCreator();
		
		Persistence.generateSchema("sos", null);	
				
	}
	
	
}
