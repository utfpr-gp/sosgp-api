package org.gp.sos.resource;

import javax.ws.rs.Path;

import org.gp.sos.model.ParticularService;
import org.gp.sos.model.service.ParticularServiceService;

@Path("/particular-services")
public class ParticularServiceResource extends AbstractResource<Integer, ParticularService> {

	public ParticularServiceResource() {
		service = new ParticularServiceService();
	}
}
