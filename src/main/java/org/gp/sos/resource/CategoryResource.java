package org.gp.sos.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.gp.sos.model.Category;
import org.gp.sos.model.service.CategoryService;


@Path("/categorias")
public class CategoryResource extends AbstractResource<Integer, Category>{ 
	
	private static final Logger LOGGER = Logger.getLogger(CategoryResource.class);	
	
	//CategoryService service;
	
	public CategoryResource() {
		service = new CategoryService();
	}	
	
	/*
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll() {
		try {
			return Response
			.status(200)
			.entity(service.findAll())
			.build();			
		} 
		catch (Exception ex) {			
			ex.printStackTrace();			
			throw new WebApplicationException(500);
		}
	}
	
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces(MediaType.APPLICATION_JSON)
	public Response save(Category entity) {
		try {
			System.out.println("Persistindo " + entity.getName());
			service.save(entity);
			
			System.out.println("Persistiu");
			return Response
					.status(200)
					.entity(entity)
					.build();	
			
		} 
		catch (Exception ex) {			
			throw new WebApplicationException(500);
		}
	}
	
	@PUT
	@Consumes({MediaType.APPLICATION_JSON})
	public Response update(Category entity) {
		try {
			
			service.update(entity);
			
			return Response
					.status(200)
					.entity(entity)
					.build();	
			
		} 
		catch (Exception ex) {
			LOGGER.error(ex);
			throw new WebApplicationException(500);
		}
	}

	@DELETE
	@Path("/{id}")
	@Consumes({MediaType.APPLICATION_JSON})
	public Response delete(@PathParam("id") Integer id) {
		try {
			
			service.deleteById(id);
			
			return Response
					.status(200)					
					.build();
			
		} 
		catch (Exception ex) {
			LOGGER.error(ex);
			throw new WebApplicationException(500);
		}
	}

	*/
	
}
