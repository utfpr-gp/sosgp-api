package org.gp.sos.resource;

import javax.ws.rs.Path;

import org.gp.sos.model.City;
import org.gp.sos.model.service.CityService;

@Path("/cities")
public class CityResource extends AbstractResource<String, City>{
		
	public CityResource() {
		service = new CityService();
	}

}
