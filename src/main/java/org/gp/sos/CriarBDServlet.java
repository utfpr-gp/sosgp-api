package org.gp.sos;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.gp.sos.model.Category;
import org.gp.sos.model.City;
import org.gp.sos.model.Neighborhood;
import org.gp.sos.model.ParticularService;
import org.gp.sos.model.service.CategoryService;
import org.gp.sos.model.service.CityService;
import org.gp.sos.model.service.ParticularServiceService;

/**
 * Servlet implementation class CriarBDServlet
 */
@WebServlet("/criar")
public class CriarBDServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
    public CriarBDServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		criarBD();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		criarBD();
	}
	
	private void criarBD(){		
		
		CategoryService categoryService = new CategoryService();
		List<String> categories = Arrays.asList("Mecânico", "Carpinteiro", "Eletricista", "Encanador", "Guincho", "Dedetizador", "Desentupidor", "Pedreiro", "Pintor", "Mudanças e Carreto", "Jardineiro", "Motoboy", "Chaveiro", "Técnico em Computação"); 
		Category category = null;
		for(String c : categories){
			category = new Category(c);
			categoryService.save(category);
		}		
		
		CityService cityService = new CityService();
		City city = new City("Guarapuava");
		city.setNeighborhoods(listGuarapuavaNeighborhoods());		
		
		cityService.save(city);
		
		ParticularServiceService particularServiceService = new ParticularServiceService();
		ParticularService particular = new ParticularService("Maria", "000", "blablabla", category);
		particular.setNeighborhood(city.getNeighborhoods().get(0)); 
		particularServiceService.save(particular);
		
	}	
	
	public List<Neighborhood> listGuarapuavaNeighborhoods(){
		List<Neighborhood> neighborhoods = new ArrayList<>();
		neighborhoods.add(new Neighborhood("Aldeia"));
		neighborhoods.add(new Neighborhood("Centro"));
		neighborhoods.add(new Neighborhood("Jordão"));
		neighborhoods.add(new Neighborhood("Batel"));
		neighborhoods.add(new Neighborhood("Santa Cruz"));
		neighborhoods.add(new Neighborhood("Santana"));
		neighborhoods.add(new Neighborhood("Boqueirão"));
		neighborhoods.add(new Neighborhood("Vila Bela"));
		neighborhoods.add(new Neighborhood("Cascavel"));
		neighborhoods.add(new Neighborhood("Alto Cascavel"));
		neighborhoods.add(new Neighborhood("Jardim das Américas"));
		neighborhoods.add(new Neighborhood("São Cristóvão"));
		neighborhoods.add(new Neighborhood("Alto da XV"));
		neighborhoods.add(new Neighborhood("Morro Alto"));
		neighborhoods.add(new Neighborhood("Bonsucesso"));
		neighborhoods.add(new Neighborhood("Dos Estados"));
		neighborhoods.add(new Neighborhood("Vila Carli"));
		neighborhoods.add(new Neighborhood("Conradinho"));
		neighborhoods.add(new Neighborhood("Primavera"));
		neighborhoods.add(new Neighborhood("Trianon"));
		neighborhoods.add(new Neighborhood("Insdustrial"));
		
		return neighborhoods;
	}

}
