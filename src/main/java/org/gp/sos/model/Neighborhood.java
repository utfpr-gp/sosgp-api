package org.gp.sos.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="neighborhood")
public class Neighborhood implements Serializable{

	@Id
	private String name;
	
	private String city;
		
	public Neighborhood() {}
	
	public Neighborhood(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}	
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
}
