package org.gp.sos.model.service;

import org.gp.sos.model.ParticularService;
import org.gp.sos.model.dao.ParticularServiceDAO;

public class ParticularServiceService extends AbstractService<Integer, ParticularService>{
	
	public ParticularServiceService() {
		dao = new ParticularServiceDAO();
	}
}
