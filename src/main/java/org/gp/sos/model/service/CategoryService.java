package org.gp.sos.model.service;

import org.gp.sos.model.Category;
import org.gp.sos.model.dao.CategoryDAO;

public class CategoryService extends AbstractService<Integer, Category>{
     
    public CategoryService(){        
        dao = new CategoryDAO();
    }   
}
