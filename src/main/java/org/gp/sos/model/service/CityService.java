package org.gp.sos.model.service;

import org.gp.sos.model.City;
import org.gp.sos.model.dao.CityDAO;

public class CityService extends AbstractService<String, City>{ 
	
	public CityService() {
		dao = new CityDAO();
	}
}
